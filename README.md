![Alt-Text](/source/images/header.jpg)

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)  [![Code Climate](https://codeclimate.com/github/codeclimate/codeclimate/badges/gpa.svg)](https://codeclimate.com/github/codeclimate/codeclimate) 
[![AppVeyor](https://img.shields.io/appveyor/ci/stevepeak/example-csharp.svg)](https://ci.appveyor.com/project/stevepeak/example-csharp/branch/master)
[![codecov](https://codecov.io/gh/codecov/example-csharp/branch/master/graph/badge.svg)](https://codecov.io/gh/codecov/example-csharp)

# Containalytics™ (Beta) (Migration from Github is ongoing)

***"Cloud container data analytics, statistical modeling, and machine learning on distributed databases.  An opensource alternative to SPSS, SAS, Databricks, PowerBI, Tableau and Alteryx".***

***"Free alternative to proprietary platforms like Dataiku, Code Ocean, and Domino Data Lab"***

Note: The Flutter 3 UI refactor is ongoing, legacy Angular and React portions are in the migration process.

Upstream tracker: https://opendatahub.io/landscape

![Alt-Text](/source/images/cross-platform.JPG)

Containalytics™ is a opensource software environment for statistical analytics and graphics. The platform's analytic outputs (R, Python, Spark) are reproducible by using Docker containers and tools such as "RStudio Package Manager", "RENV", and "CONDA/PIP".

Containalytics allows the end-user to integrate a broad spectrum of high quality opensource software.  Each tool is made available depending on use case, without requiring the end user to seek out, individually install, configure, update, and integrate disparate tooling into new or existing analysis workflows.  

Each component dependency is pulled from each respective upstream project and/or docker repository, and no upstream code is ever modified. Different industries and user roles have varied needs; as such the platform is fully customizable:  Analytic containers can be added, removed, or rolled back to earlier versions depending on host infrastructure and the applicable decision tree based reporting template.

## Use Cases: Data summarization, visualization, classification and prediction. 

***Guidance should be sought from a statistician when using Containalytics for a research study, especially prior to submitting the study for publication.***

- Customer churn prediction

- Drug efficacy and personalized healthcare treatment

- Patient admissions prediction

- Medical practice management and optimization

- Patient segmentation

- Personalized financial service or product offers

- Underwriting and risk assessment 

- Fraud detection 

- Cyber and infrastructure security threat detection and prediction

- Algorithmic trading and financial forecasting

- Influencer analysis and targeted customer marketing

- Clickstream analytics

- Loss control and claim management

- Survey evaluation

- Predictive industrial maintenance (service life prediction)

- Supply chain optimization

- Economic planning and fiscal policy           

- Scientific research: chemistry, bioinformatics, neuroscience...

## Reference projects making use of the Containalytics platform:

- Machine Learning for Warehouse Capacity Prediction Using Python and Scikit-Learn

- Balance-Learner-ML: A Python and R Toolset for Resolving Imbalanced Datasets in Machine Learning

- PySpark Feature Importance Analysis for Disease Progression Prediction via Generalized Discriminant Analysis and Least Square Support Vector Machine

- Applying Python Machine Learning to Assess and Predict Product and Service Quality

- Qualia-Spark: A Quality Assessment Software Pipeline Enabled by Support Vector Machines, Artificial Neural Networks, Stochastic Gradient Descent, K-Nearest Neighbors, Decision Trees, and Random Forests

- Insurance Risk and Cost Prediction Enabled by Python Regression and Deep Learning Model Computation Pipelines

## All statistical model code is accessible via the Code Editor
![Alt-Text](/source/images/editor.JPG)

## Quality Assurance
![Alt-Text](/source/images/risk.png)

Software and package versions will change over time, and maintaining installations will become more complex. Containalytics solves this issue for R code by following a defined process that checks and tests package integrity with dependencies via the R validation hub.

Containalytics functions/components pass a series of technical checks to ensure:

- successfull code execution
- passing tests
- inter-package compatibility

The Containalytics R validation hub process includes:

- regular package updates
- predictable release rate
- quantifying the size and organization of the code base
- formal bug tracking
- community usage and testing
- average number of downloads during in the last 12 months
- level of code coverage by formal testing frameworks

![Alt-Text](/source/images/pipelines.JPG)

## ML Model Management
![Alt-Text](/source/images/rubicon.png)
Capture model development in a reproducible way and tie results directly back to the model under version control.
- https://github.com/capitalone/rubicon


## Q/A Tooling

![Alt-Text](/source/images/secscans.JPG)

![Alt-Text](/source/images/pipelineslist.JPG)

###  R Statistics Testing:
testthat: https://cran.r-project.org/web/packages/testthat/index.html

lintr: https://cran.r-project.org/web/packages/lintr/index.html

### Python Testing:
Functional testing for applications and libraries.

Pytest: https://docs.pytest.org/en/6.2.x/contents.html


## Frontend web client components
While currently written in React.js and Angular, exploration is currently underway to port 
some elements to WASM/web-assembly (Godot + .NET Core Blazor, and Google's Flutter).


## Frontend desktop client orchestration

The Godot desktop engine launches the various components and allows for user configuration.  This is similar
To how 'Anaconda Navigator' configures and then launches an end users Python and R applications.

## Backend Components and REST + GRAPHQL API

The backend API consists of Python 3 Flask, Node.JS, and .NET Core (http://accord-framework.net/) services interfacing with a MySQL database.
A SAS statistical reference is provided for key functions, but SAS code is not executed by the platform.

The REST API endpoints are documented via OpenAPI (https://swagger.io/specification/).

Input data (CSV, Database connections) is analysed via Python, R (Shiny/Rstudio/Bluesky/Jamovi), and the KNIME Analytics Platform.

The application's cross platform CLI is written in C#, and the application's deployment/operations tool is written in Go/Golang and Python (Ansible).
.Net (Maui) mobile implementations for iOS and Android are anticipated for Fall 2021.

The application is compatible with the following AI/ML frameworks: 
TensorFlow, PyTorch, Apache MXNet, Chainer, Keras, Gluon, Horovod, Scikit-learn, and Deep Graph Library

## WEB-STATS - Frontend Analytic Jamovi Component

![Alt-Text](/source/images/containalytics-R-jm.png)

## DATA-STICHERY - Backend Analytic NiFi Component

![Alt-Text](/source/images/NiFi.PNG)

## DATA-STICHERY - Desktop Analytic KNIME Component
![Alt-Text](/source/images/KNIME-stages.png)

## NOTEBOOKS - Frontend Jupyterlab Component

![Alt-Text](/source/images/jupyterlab.png)

## SQL-VIEW - Frontend Redash Component

![Alt-Text](/source/images/redash.png)

## LaTeX-TYPESETTER - TexStudio & MiKTeX Report Publishing Component

- Document preparation system for high-quality typesetting. 

- Used for medium-to-large technical or scientific documents, but can be used for almost any form of publishing.

![Alt-Text](/source/images/latex.jpg)

## Remote Desktop
- xRDP link to container's user interface
Multi User xrdp remote desktop on Ubuntu. 
Copy/Paste is implemented. Users can re-login in the same XFCE session.

- Run desktop tooling and IDEs like DBeaver and Jetbrains Dataspell

![Alt-Text](/source/images/dataspell.JPG)

![Alt-Text](/source/images/py_ds_debug_cell.png)

![Alt-Text](/source/images/DBeaver.png)

## Multi-Terabyte analytic processing
- For datasets larger than 1 million rows and/or data having hundreds of columns variables,
the platform allows for use of the Apache Spark 3.x runtime.

- For cloud, hybrid, and onpremise container infrastructure scenarios: Helm charts can be deployed to HPE Ezmeral, IBM/RedHat Openshift, and generic Kubernetes platform environments.

![Alt-Text](/source/images/pipeline-dask-spark.jpg)


## Warehouse and streaming data integration

![Alt-Text](/source/images/data-integration.jpg)


## Database and clustered computation
- Install MariaDB(MySQL)
- Install Yugabyte (clustered PostgreSQL)
- Install Apache Spark 3.x (clustered ANSI SQL, clustered R + Python)

## Integrated Development Environments (IDE)
- Visual Studio Code (modify the C# functional and object-oriented code extensions)

## Datascience notebook interface
- Install JupyterLab (Statistics Notebook)

## R statistical analytics tooling and dashboard
- Install Rstudio (R IDE and package manager) + Shiny application platform
- Install Bluesky (Point and click R Statistics)
- Install Jamovi (Point and click interface that can output SAS files)

## Dataflows
- Install NiFi (Web UI)
- Install KNIME (Desktop UI)

## SQL driven plots and graphs
- Install Redash

## Virtual Machine Deployment
- Install virtualbox:
https://www.virtualbox.org/wiki/Downloads

- Deploy VBOX images fleet
- Bootstrap base file: https://app.vagrantup.com/ubuntu/boxes/focal64

## Docker-Compose (Deprecated)
- Note: As of 2021 Docker, Inc. has introduced enterprise usage restrictions.  OCI compliant alternatives are available from other vendors.
- Switch to an OCI distribution and use "nerdctl compose" as the new opensource alternative.

## Kubernetes (Preferred Deployment)
- For docker license constrained users please migrate to opensource Rancher Desktop, Openshift, or a  native cloud provider offering.
- https://github.com/rancher-sandbox/rancher-desktop

![ALT-TEXT](/source/images/image-01.jpeg)

## Run the containers individually

Launch MySQL database service: docker run --name db-csv -v /my/own/datadir:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:8.0.20

Download this application's Quick Start All-In-One Docker image: 
docker pull docker.pkg.github.com/containalytics/containalytics:VERSION

## Infrastructure-as-code: 

![Alt-Text](/source/images/components.jpg)

The codebase was previously maintained and built via Github Actions and is being transitioned to Gitlab.

Use of a Kubernetes Helm chart is optional, a manual kubectl option is also available for single server node operation.

Docker/OCI images are based on Ubuntu/Debian Linux.

![Alt-Text](/source/images/imagerepo.JPG)

Service checks, updates, and platform configuration is managed via Gitlab and Kubernetes helm charts/operators.

Cloud instances are deployed via Terraform for cross-service provider compatibility with Microsoft Azure, Google GCP, Amazon AWS, and Oracle cloud.

## Service Hosting

The primary application and dependencies are hosted on Oracle Cloud Infrastructure:

- Compute: 2 virtual machines with 1 OCPU and 1 GB memory each.

- Storage: 2 Block Volumes, 100 GB total. 10 GB Object Storage. 10 GB Archive Storage.

- Additional Services: Load Balancer: 1 instance, 10 Mbps bandwidth.

- Monitoring: Up to 500 million ingestion datapoints, 1 billion retrieval datapoints. 

- Outbound Data Transfer Limit: 10 TB per month.

### Google Cloud (GCP), MS Azure, Amazon Cloud (AWS)
![Alt-Text](/source/images/cloud.JPG)
- Provides service provider redundancy, backups, and specialty compute capacity.

## Feature Matrix


| Compatibility                                             | BlueSky | Jamovi | KNIME | RStudio | JASP | Apache NiFi | Spark SQL | Yugabyte | Apache Spark | JupyterLab | Redash|
| :---------------------------------------------------------|:-------:|:------:|:-----:|:-------:|:----:|:-----------:|:----:|:--------:|:--------------:|:----------:|:-:|
| Web-app / server                                          |         | x      |       |         |      |             |      |          |                |   x        | x |
| CLI / server                                              | | |x| | | | | | | |
| Integrated postgresql script execution                    | | | | | | | |x| | |
| Integrated sql  execution                         | | | | | | |x| | | |
| External sql execution                                    | | |x| | | | | | | |
| Execute python 3                                          | | |x| | | | | | |x|
| Execute R                                                 | | | |x| | | | | | |
| Execute clustered R                                       | | | | | | | | |x| |
| Menu driven modern UI                                     | |x| | |x| | | | | |
| Dataflow UI                                               | | | | | |x| | | | |
| Advanced statistics functions                             |x| | | | | | | | | |
| Notebook interface                                        | | | | | | | | | |x|
| Sql UI interface                                          | | | | | | | | | |X|

## Roadmap of future features

- Integrate notebook search: https://github.com/elsevierlabs-os/NotebookDiscovery

- Compatibility with YugabyteDB, a distributed SQL database that supports the best features of both a traditional RDBMS like MySQL and PostgreSQL, but adds the scalability, speed, and resiliency of a NoSQL datastore.

- Enhanced Kubernetes compatibility via Helm charts and Operators.

- Zeppelin notebooks

- Enable docker-swarm mode for horizontal cross server scalability for scenarios where Kubernetes is not available.

- Package R/Shiny components as electron desktop apps once project Photon supports CI/CD (i.e. no mandatory Rstudio UI dependencies).

- Port and benchmark desktop clients for Flutter, Qt5, Electron, NW.js, and React Native.

- Add tutorials, docs, and additional test datasets once milestone v1.0 is complete.

## Contact the lead developer Alexander Manley:
containalytics@gmail.com
