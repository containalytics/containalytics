Terraform cloud configuration: 
Defines infrastructure as code to manage the full lifecycle.  Creates new resources, manages existing ones, and destroys resources that are no longer needed.
